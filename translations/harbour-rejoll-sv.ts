<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv" sourcelanguage="en_US">
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Version:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Copyright:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licens:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Källkod:</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="vanished">Erkännanden</translation>
    </message>
    <message>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation type="vanished">Översättningar: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="31"/>
        <source>Translations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>%1:</source>
        <comment>%1 is the native language name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailsPanel</name>
    <message>
        <source>Unit Properties</source>
        <translation type="vanished">Enhetsegenskaper</translation>
    </message>
    <message>
        <source>Service Properties</source>
        <translation type="vanished">Tjänstegenskaper</translation>
    </message>
    <message>
        <source>Timer Properties</source>
        <translation type="vanished">Timeregenskaper</translation>
    </message>
    <message>
        <source>Mount Properties</source>
        <translation type="vanished">Monteringsegenskaper</translation>
    </message>
    <message>
        <source>Path Properties</source>
        <translation type="vanished">Sökvägsegenskaper</translation>
    </message>
    <message>
        <source>Unit name copied to clipboard</source>
        <translation type="vanished">Enhetsnamn kopierat till urklipp</translation>
    </message>
    <message>
        <source>Long press again to copy detailed info</source>
        <translation type="vanished">Långtryck igen för att kopiera detaljerad info</translation>
    </message>
    <message>
        <source>Unit details copied to clipboard</source>
        <translation type="vanished">Enhetsdetaljer kopierat till urklipp</translation>
    </message>
    <message>
        <source>Other Names</source>
        <translation type="vanished">Andra namn</translation>
    </message>
    <message>
        <source>Dependencies</source>
        <translation type="vanished">Beroenden</translation>
    </message>
    <message>
        <source>Unit File</source>
        <translation type="vanished">Enhetsfil</translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="204"/>
        <source>Copy Path</source>
        <translation>Kopiera sökväg</translation>
    </message>
    <message>
        <source>Unit file path copied to clipboard</source>
        <translation type="vanished">Enhetsfilens sökväg kopierad till urklipp</translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="213"/>
        <source>Show Content</source>
        <translation>Visa innehåll</translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="209"/>
        <source>%1 copied to clipboard</source>
        <translation>%1 kopierad till urklipp</translation>
    </message>
    <message>
        <source>unit details</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="vanished">Enhetsdetaljer</translation>
    </message>
    <message>
        <source>unit name</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="vanished">Enhetsnamn</translation>
    </message>
    <message>
        <source>unit file path</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="vanished">Enhetsfilens sökväg</translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="77"/>
        <source>File Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="199"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="209"/>
        <source>file path</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="unfinished">filsökväg</translation>
    </message>
</context>
<context>
    <name>FileDetail</name>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="13"/>
        <source>Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="14"/>
        <source>Author</source>
        <translation type="unfinished">Utvecklare</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="15"/>
        <source>Caption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="16"/>
        <source>Collapsed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="17"/>
        <source>DB Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="18"/>
        <source>DB Mod. Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="19"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="20"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="21"/>
        <source>File mtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="22"/>
        <source>DB path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="23"/>
        <source>Mod. Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="24"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="25"/>
        <source>Charset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="26"/>
        <source>PC Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="27"/>
        <source>DB udi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="28"/>
        <source>DB aptg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="29"/>
        <source>Recipient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="30"/>
        <source>Relevancy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="31"/>
        <source>Sig</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="32"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="33"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LicenseListPart</name>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="119"/>
        <source>License text</source>
        <translation>Licenstext</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="31"/>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="28"/>
        <source>Refresh</source>
        <translation type="unfinished">Uppdatera</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="42"/>
        <source>Advanced Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="49"/>
        <source>Hide search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="49"/>
        <source>Quick Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>ReJoll</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>%Ln hit(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="63"/>
        <source>something to search for</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Opal.About</name>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="55"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="84"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="100"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="22"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="31"/>
        <source>Development</source>
        <translation>Utveckling</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="103"/>
        <source>show contributors</source>
        <translation>visa medverkande</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="108"/>
        <location filename="../qml/modules/Opal/About/private/ChangelogPage.qml" line="16"/>
        <source>Changelog</source>
        <translation>Ändringslogg</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="129"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="134"/>
        <source>show license(s)</source>
        <translation>
            <numerusform>visa licens</numerusform>
            <numerusform>visa licenser</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="114"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="50"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="58"/>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="50"/>
        <source>Source Code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="21"/>
        <source>Contributors</source>
        <translation>Medverkande</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/InfoSection.qml" line="15"/>
        <source>show details</source>
        <translation>visa detaljerat</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="31"/>
        <source>License(s)</source>
        <translation>
            <numerusform>Licens</numerusform>
            <numerusform>Licenser</numerusform>
        </translation>
    </message>
    <message>
        <source>Please refer to &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation type="vanished">Se &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="111"/>
        <source>Translations</source>
        <translation>Översättning</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="41"/>
        <source>Note: please check the source code for most accurate information.</source>
        <translation>Notis: Kontrollera källkoden för mest korrekt information.</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="37"/>
        <source>Acknowledgements</source>
        <translation>Bekräftelser</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="44"/>
        <source>Thank you!</source>
        <translation>Tack!</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="123"/>
        <source>Donations</source>
        <translation>Donationer</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="15"/>
        <source>External Link</source>
        <translation>Extern länk</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="32"/>
        <source>Open in browser</source>
        <translation>Öppna i webbläsare</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="41"/>
        <source>Copy to clipboard</source>
        <translation>Kopiera till urklipp</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="25"/>
        <source>Download license texts</source>
        <translation>Ladda ner licenstexter</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="105"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="51"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="57"/>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="47"/>
        <source>Homepage</source>
        <translation>Hemsida</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="48"/>
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="31"/>
        <source>Details</source>
        <translation>Detaljerat</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="36"/>
        <source>Copied to clipboard: %1</source>
        <translation>Kopierat till urklipp: %1</translation>
    </message>
    <message>
        <source>Version %1 (%2)</source>
        <translation type="vanished">Version %1 (%2)</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="120"/>
        <source>Please refer to &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>Se &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/ChangelogNews.qml" line="60"/>
        <source>News</source>
        <translation>Nyheter</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/ChangelogNews.qml" line="61"/>
        <source>Changes since version %1</source>
        <translation>Ändrat sedan version %1</translation>
    </message>
</context>
<context>
    <name>Opal.About.Common</name>
    <message>
        <source>Programming</source>
        <translation type="vanished">Programmering</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation type="vanished">Översättningar</translation>
    </message>
    <message>
        <source>Icon Design</source>
        <translation type="vanished">Ikondesign</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="vanished">Svenska</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="vanished">Polska</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="vanished">Tyska</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="vanished">Franska</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="vanished">Kinesiska</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">Engelska</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="vanished">Italienska</translation>
    </message>
    <message>
        <source>Finnish</source>
        <translation type="vanished">Finska</translation>
    </message>
    <message>
        <source>Norwegian</source>
        <translation type="vanished">Norska</translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="vanished">Data</translation>
    </message>
    <message numerus="yes">
        <source>Data License(s)</source>
        <translation type="vanished">
            <numerusform>Datalicens</numerusform>
            <numerusform>Datalicenser</numerusform>
        </translation>
    </message>
    <message>
        <source>Third-party libraries</source>
        <translation type="vanished">Tredjeparts bibliotek</translation>
    </message>
    <message>
        <source>Data License</source>
        <translation type="vanished">Datalicens</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/DonationsGroup.qml" line="7"/>
        <source>If you want to support my work, you can buy me a cup of coffee.</source>
        <translation>Om du vill stödja mitt arbete, kan du bjuda mig på en kopp kaffe.</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/DonationsGroup.qml" line="8"/>
        <source>You can support this project by contributing, or by donating using any of these services.</source>
        <translation>Du kan stödja projektet genom kodbidrag eller donation med hjälp av dessa tjänster.</translation>
    </message>
    <message>
        <source>Latvian</source>
        <translation type="vanished">Lettiska</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/DonationsGroup.qml" line="9"/>
        <source>Your contributions to translations or code would be most welcome.</source>
        <translation>Kodbidrag eller översättningar är väldigt välkommet.</translation>
    </message>
    <message>
        <source>Estonian</source>
        <translation type="vanished">Estniska</translation>
    </message>
    <message>
        <source>Terms of Use</source>
        <translation type="vanished">Villkor för användning</translation>
    </message>
    <message>
        <source>Author</source>
        <translation type="vanished">Utvecklare</translation>
    </message>
    <message numerus="yes">
        <source>Author(s)</source>
        <translation type="vanished">
            <numerusform>Utvecklare</numerusform>
            <numerusform>Utvecklare</numerusform>
        </translation>
    </message>
    <message>
        <source>Maintainer</source>
        <translation type="vanished">Ansvarig</translation>
    </message>
    <message numerus="yes">
        <source>Maintainer(s)</source>
        <translation type="vanished">
            <numerusform>Ansvarig</numerusform>
            <numerusform>Ansvariga</numerusform>
        </translation>
    </message>
    <message>
        <source>Czech</source>
        <translation type="vanished">Tjeckiska</translation>
    </message>
    <message>
        <source>Greek</source>
        <translation type="vanished">Grekiska</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="vanished">Spanska</translation>
    </message>
    <message>
        <source>Hungarian</source>
        <translation type="vanished">Ungerska</translation>
    </message>
    <message>
        <source>Indonesian</source>
        <translation type="vanished">Indonesiska</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="vanished">Ryska</translation>
    </message>
    <message>
        <source>Dutch</source>
        <translation type="vanished">Nederländska</translation>
    </message>
    <message>
        <source>Slovak</source>
        <translation type="vanished">Slovakiska</translation>
    </message>
    <message>
        <source>Turkish</source>
        <translation type="vanished">Turkiska</translation>
    </message>
</context>
<context>
    <name>ResultItem</name>
    <message>
        <location filename="../qml/components/ResultItem.qml" line="49"/>
        <source>-- no title --</source>
        <comment>replacement for an empty field</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="145"/>
        <source>Settings</source>
        <comment>page title</comment>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="vanished">Program</translation>
    </message>
    <message>
        <source>Show Notifications</source>
        <translation type="vanished">Visa aviseringar</translation>
    </message>
    <message>
        <source>List Cleared.</source>
        <translation type="vanished">Listan rensad.</translation>
    </message>
    <message>
        <source>Sticky Notifications</source>
        <translation type="vanished">Fasta aviseringar</translation>
    </message>
    <message>
        <source>Advanced:</source>
        <translation type="vanished">Avancerat:</translation>
    </message>
    <message>
        <source>min</source>
        <translation type="vanished">min</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="92"/>
        <location filename="../qml/pages/SettingsPage.qml" line="93"/>
        <source>Enable %1</source>
        <translation type="unfinished">Aktivera %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="102"/>
        <location filename="../qml/pages/SettingsPage.qml" line="103"/>
        <source>Disable %1</source>
        <translation type="unfinished">Inaktivera %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="111"/>
        <source>Start %1</source>
        <translation type="unfinished">Starta %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="119"/>
        <source>Stop %1</source>
        <translation type="unfinished">Stoppa %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="127"/>
        <source>Restart %1</source>
        <translation type="unfinished">Starta om %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="134"/>
        <source>About</source>
        <translation type="unfinished">Om</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="195"/>
        <source>If enabled, the database will be updated twice a day, and 1 hour after a reboot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="obsolete">Stoppa</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="obsolete">Starta</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="149"/>
        <source>Previews</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="156"/>
        <source>Extract file contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="157"/>
        <source>If enabled, the app will try to generate text previews when fetching results. This may slow down result retrieval performance if %1 is large.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="157"/>
        <location filename="../qml/pages/SettingsPage.qml" line="170"/>
        <source>Batch Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="163"/>
        <source>Queries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="181"/>
        <source>Database Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="192"/>
        <source>indexing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="193"/>
        <source>waiting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="194"/>
        <source>stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="212"/>
        <source>Run now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="186"/>
        <source>Scheduler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="187"/>
        <source>%1, (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="188"/>
        <source>enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="188"/>
        <source>disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="207"/>
        <source>Indexer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnitItem</name>
    <message>
        <source>Ignore this session</source>
        <translation type="vanished">Undanta denna session</translation>
    </message>
    <message>
        <source>Ignore permanently</source>
        <translation type="vanished">Undanta permanent</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">Starta om</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="vanished">Aktivera</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation type="vanished">Inaktivera</translation>
    </message>
    <message>
        <source>Mount</source>
        <translation type="vanished">Montera</translation>
    </message>
    <message>
        <source>Unmount</source>
        <translation type="vanished">Avmontera</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="vanished">Starta</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Stoppa</translation>
    </message>
    <message>
        <source>No actions available.</source>
        <translation type="vanished">Inga åtgärder tillgängliga.</translation>
    </message>
    <message>
        <source>Remount</source>
        <translation type="vanished">Återmontera</translation>
    </message>
    <message>
        <source>next</source>
        <translation type="vanished">nästa</translation>
    </message>
    <message>
        <source>Enabling %1</source>
        <translation type="vanished">Aktiverar %1</translation>
    </message>
    <message>
        <source>Disabling %1</source>
        <translation type="vanished">Inaktiverar%1</translation>
    </message>
    <message>
        <source>Stopping %1</source>
        <translation type="vanished">Stoppar %1</translation>
    </message>
    <message>
        <source>Restarting %1</source>
        <translation type="vanished">Startar om %1</translation>
    </message>
</context>
<context>
    <name>UnitPage</name>
    <message>
        <source>About</source>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Inställningar</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Uppdatera</translation>
    </message>
    <message>
        <source>Switch to %1</source>
        <translation type="vanished">Växla till %1</translation>
    </message>
</context>
<context>
    <name>ViewFile</name>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="41"/>
        <source>Share %1…</source>
        <translation type="unfinished">Dela %1…</translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="53"/>
        <source>Share as Text…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="65"/>
        <source>Open…</source>
        <translation type="unfinished">Öppna…</translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="72"/>
        <location filename="../qml/pages/ViewFile.qml" line="73"/>
        <source>Copy %1 to clipboard</source>
        <translation type="unfinished">Kopiera %1 till urklipp</translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="72"/>
        <source>file path</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="unfinished">filsökväg</translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="72"/>
        <location filename="../qml/pages/ViewFile.qml" line="73"/>
        <source>%1 copied to clipboard</source>
        <translation type="unfinished">%1 kopierad till urklipp</translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="73"/>
        <source>file contents</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="unfinished">innehåll</translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="41"/>
        <source>File</source>
        <comment>argument for &apos;Share&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewUnitFile</name>
    <message>
        <source>Copy file path to Clipboard</source>
        <translation type="vanished">Kopiera filsökväg till urklipp</translation>
    </message>
    <message>
        <source>Copy contents to Clipboard</source>
        <translation type="vanished">Kopiera innehåll till urklipp</translation>
    </message>
    <message>
        <source>Open…</source>
        <translation type="vanished">Öppna…</translation>
    </message>
    <message>
        <source>Copy %1 to clipboard</source>
        <translation type="vanished">Kopiera %1 till urklipp</translation>
    </message>
    <message>
        <source>file path</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="vanished">filsökväg</translation>
    </message>
    <message>
        <source>%1 copied to clipboard</source>
        <translation type="vanished">%1 kopierad till urklipp</translation>
    </message>
    <message>
        <source>file contents</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="vanished">innehåll</translation>
    </message>
    <message>
        <source>Share %1…</source>
        <translation type="vanished">Dela %1…</translation>
    </message>
</context>
<context>
    <name>harbour-sailord</name>
    <message>
        <source>Enabling %1</source>
        <translation type="vanished">Aktiverar %1</translation>
    </message>
    <message>
        <source>Disabling %1</source>
        <translation type="vanished">Inaktiverar%1</translation>
    </message>
    <message>
        <source>Restarting %1</source>
        <translation type="vanished">Startar om %1</translation>
    </message>
    <message>
        <source>Stopping %1</source>
        <translation type="vanished">Stoppar %1</translation>
    </message>
    <message>
        <source>%1 successful.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation type="vanished">%1 slutfört.</translation>
    </message>
    <message>
        <source>Success:</source>
        <translation type="vanished">Slutfört:</translation>
    </message>
    <message>
        <source>%1 failed.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation type="vanished">%1 misslyckades.</translation>
    </message>
    <message>
        <source>Failure:</source>
        <translation type="vanished">Misslyckat:</translation>
    </message>
    <message>
        <source>Preset %1</source>
        <translation type="vanished">Förinställ %1</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="obsolete">Aktivera</translation>
    </message>
    <message>
        <source>Enable %1</source>
        <translation type="vanished">Aktivera %1</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation type="obsolete">Inaktivera</translation>
    </message>
    <message>
        <source>Disable %1</source>
        <translation type="vanished">Inaktivera %1</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="vanished">Starta</translation>
    </message>
    <message>
        <source>Start %1</source>
        <translation type="vanished">Starta %1</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Stoppa</translation>
    </message>
    <message>
        <source>Stop %1</source>
        <translation type="vanished">Stoppa %1</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">Starta om</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <translation type="vanished">Starta om %1</translation>
    </message>
</context>
</TS>
