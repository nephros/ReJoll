<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="31"/>
        <source>Translations</source>
        <translation type="unfinished">Translations</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>%1:</source>
        <comment>%1 is the native language name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailsPanel</name>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="77"/>
        <source>File Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="199"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="204"/>
        <source>Copy Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="209"/>
        <source>%1 copied to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="209"/>
        <source>file path</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="213"/>
        <source>Show Content</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileDetail</name>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="13"/>
        <source>Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="14"/>
        <source>Author</source>
        <translation type="unfinished">Author</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="15"/>
        <source>Caption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="16"/>
        <source>Collapsed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="17"/>
        <source>DB Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="18"/>
        <source>DB Mod. Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="19"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="20"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="21"/>
        <source>File mtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="22"/>
        <source>DB path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="23"/>
        <source>Mod. Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="24"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="25"/>
        <source>Charset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="26"/>
        <source>PC Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="27"/>
        <source>DB udi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="28"/>
        <source>DB aptg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="29"/>
        <source>Recipient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="30"/>
        <source>Relevancy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="31"/>
        <source>Sig</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="32"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="33"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LicenseListPart</name>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="119"/>
        <source>License text</source>
        <translation>License text</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="28"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="31"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="42"/>
        <source>Advanced Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="49"/>
        <source>Hide search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="49"/>
        <source>Quick Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>ReJoll</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>%Ln hit(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="63"/>
        <source>something to search for</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Opal.About</name>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="55"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="84"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <source>Version %1 (%2)</source>
        <translation type="vanished">Version %1 (%2)</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="100"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="22"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="31"/>
        <source>Development</source>
        <translation>Development</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="103"/>
        <source>show contributors</source>
        <translation>show contributors</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="105"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="51"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="57"/>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="47"/>
        <source>Homepage</source>
        <translation>Homepage</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="108"/>
        <location filename="../qml/modules/Opal/About/private/ChangelogPage.qml" line="16"/>
        <source>Changelog</source>
        <translation>Changelog</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="123"/>
        <source>Donations</source>
        <translation>Donations</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="129"/>
        <source>License</source>
        <translation>License</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="134"/>
        <source>show license(s)</source>
        <translation>
            <numerusform>show license</numerusform>
            <numerusform>show licenses</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="111"/>
        <source>Translations</source>
        <translation>Translations</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="114"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="50"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="58"/>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="50"/>
        <source>Source Code</source>
        <translation>Source Code</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="21"/>
        <source>Contributors</source>
        <translation>Contributors</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="37"/>
        <source>Acknowledgements</source>
        <translation>Acknowledgements</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="44"/>
        <source>Thank you!</source>
        <translation>Thank you!</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="48"/>
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="31"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/InfoSection.qml" line="15"/>
        <source>show details</source>
        <translation>show details</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="25"/>
        <source>Download license texts</source>
        <translation>Download license texts</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="31"/>
        <source>License(s)</source>
        <translation>
            <numerusform>License</numerusform>
            <numerusform>Licenses</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="41"/>
        <source>Note: please check the source code for most accurate information.</source>
        <translation>Note: please check the source code for most accurate information.</translation>
    </message>
    <message>
        <source>Please refer to &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation type="vanished">Please refer to &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="15"/>
        <source>External Link</source>
        <translation>External Link</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="32"/>
        <source>Open in browser</source>
        <translation>Open in browser</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="36"/>
        <source>Copied to clipboard: %1</source>
        <translation>Copied to clipboard: %1</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="41"/>
        <source>Copy to clipboard</source>
        <translation>Copy to clipboard</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="120"/>
        <source>Please refer to &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>Please refer to &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/ChangelogNews.qml" line="60"/>
        <source>News</source>
        <translation>News</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/ChangelogNews.qml" line="61"/>
        <source>Changes since version %1</source>
        <translation>Changes since version %1</translation>
    </message>
</context>
<context>
    <name>Opal.About.Common</name>
    <message>
        <source>Author</source>
        <translation type="vanished">Author</translation>
    </message>
    <message numerus="yes">
        <source>Author(s)</source>
        <translation type="vanished">
            <numerusform>Author</numerusform>
            <numerusform>Authors</numerusform>
        </translation>
    </message>
    <message>
        <source>Maintainer</source>
        <translation type="vanished">Maintainer</translation>
    </message>
    <message numerus="yes">
        <source>Maintainer(s)</source>
        <translation type="vanished">
            <numerusform>Maintainer</numerusform>
            <numerusform>Maintainers</numerusform>
        </translation>
    </message>
    <message>
        <source>Programming</source>
        <translation type="vanished">Programming</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation type="vanished">Translations</translation>
    </message>
    <message>
        <source>Icon Design</source>
        <translation type="vanished">Icon Design</translation>
    </message>
    <message>
        <source>Third-party libraries</source>
        <translation type="vanished">Third-party libraries</translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="vanished">Data</translation>
    </message>
    <message>
        <source>Data License</source>
        <translation type="vanished">Data License</translation>
    </message>
    <message numerus="yes">
        <source>Data License(s)</source>
        <translation type="vanished">
            <numerusform>Data License</numerusform>
            <numerusform>Data Licenses</numerusform>
        </translation>
    </message>
    <message>
        <source>Terms of Use</source>
        <translation type="vanished">Terms of Use</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="vanished">Swedish</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="vanished">Polish</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="vanished">German</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="vanished">French</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="vanished">Chinese</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">English</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="vanished">Italian</translation>
    </message>
    <message>
        <source>Finnish</source>
        <translation type="vanished">Finnish</translation>
    </message>
    <message>
        <source>Norwegian</source>
        <translation type="vanished">Norwegian</translation>
    </message>
    <message>
        <source>Latvian</source>
        <translation type="vanished">Latvian</translation>
    </message>
    <message>
        <source>Estonian</source>
        <translation type="vanished">Estonian</translation>
    </message>
    <message>
        <source>Czech</source>
        <translation type="vanished">Czech</translation>
    </message>
    <message>
        <source>Greek</source>
        <translation type="vanished">Greek</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="vanished">Spanish</translation>
    </message>
    <message>
        <source>Hungarian</source>
        <translation type="vanished">Hungarian</translation>
    </message>
    <message>
        <source>Indonesian</source>
        <translation type="vanished">Indonesian</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="vanished">Russian</translation>
    </message>
    <message>
        <source>Dutch</source>
        <translation type="vanished">Dutch</translation>
    </message>
    <message>
        <source>Slovak</source>
        <translation type="vanished">Slovak</translation>
    </message>
    <message>
        <source>Turkish</source>
        <translation type="vanished">Turkish</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/DonationsGroup.qml" line="7"/>
        <source>If you want to support my work, you can buy me a cup of coffee.</source>
        <translation>If you want to support my work, you can buy me a cup of coffee.</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/DonationsGroup.qml" line="8"/>
        <source>You can support this project by contributing, or by donating using any of these services.</source>
        <translation>You can support this project by contributing, or by donating using any of these services.</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/DonationsGroup.qml" line="9"/>
        <source>Your contributions to translations or code would be most welcome.</source>
        <translation>Your contributions to translations or code would be most welcome.</translation>
    </message>
</context>
<context>
    <name>ResultItem</name>
    <message>
        <location filename="../qml/components/ResultItem.qml" line="49"/>
        <source>-- no title --</source>
        <comment>replacement for an empty field</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="92"/>
        <location filename="../qml/pages/SettingsPage.qml" line="93"/>
        <source>Enable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="102"/>
        <location filename="../qml/pages/SettingsPage.qml" line="103"/>
        <source>Disable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="111"/>
        <source>Start %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="119"/>
        <source>Stop %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="127"/>
        <source>Restart %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="134"/>
        <source>About</source>
        <translation type="unfinished">About</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="145"/>
        <source>Settings</source>
        <comment>page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="149"/>
        <source>Previews</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="156"/>
        <source>Extract file contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="157"/>
        <source>If enabled, the app will try to generate text previews when fetching results. This may slow down result retrieval performance if %1 is large.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="157"/>
        <location filename="../qml/pages/SettingsPage.qml" line="170"/>
        <source>Batch Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="163"/>
        <source>Queries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="181"/>
        <source>Database Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="186"/>
        <source>Scheduler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="187"/>
        <source>%1, (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="188"/>
        <source>enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="188"/>
        <source>disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="192"/>
        <source>indexing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="193"/>
        <source>waiting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="194"/>
        <source>stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="195"/>
        <source>If enabled, the database will be updated twice a day, and 1 hour after a reboot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="207"/>
        <source>Indexer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="212"/>
        <source>Run now</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewFile</name>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="41"/>
        <source>Share %1…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="41"/>
        <source>File</source>
        <comment>argument for &apos;Share&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="53"/>
        <source>Share as Text…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="65"/>
        <source>Open…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="72"/>
        <location filename="../qml/pages/ViewFile.qml" line="73"/>
        <source>Copy %1 to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="72"/>
        <source>file path</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="72"/>
        <location filename="../qml/pages/ViewFile.qml" line="73"/>
        <source>%1 copied to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="73"/>
        <source>file contents</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
