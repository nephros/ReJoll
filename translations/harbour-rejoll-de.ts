<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_CH">
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation type="vanished">Über</translation>
    </message>
    <message>
        <source>What&apos;s %1?</source>
        <translation type="vanished">Was ist %1?</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Version:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Urheberrecht:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Lizenz:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Quellcode:</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="vanished">Danksagungen</translation>
    </message>
    <message>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation type="vanished">%1 Übersetzung</translation>
    </message>
    <message>
        <source> </source>
        <translation type="vanished"> </translation>
    </message>
    <message>
        <source>%1 is ..</source>
        <translation type="vanished">%! ist ein ...</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="31"/>
        <source>Translations</source>
        <translation>Übersetzungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>%1:</source>
        <comment>%1 is the native language name</comment>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>DetailsPanel</name>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="77"/>
        <source>File Properties</source>
        <translation>Dateieigenschaften</translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="199"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="204"/>
        <source>Copy Path</source>
        <translation>Pfad kopieren</translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="209"/>
        <source>%1 copied to clipboard</source>
        <translation>%1 in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="209"/>
        <source>file path</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation>Dateipfad</translation>
    </message>
    <message>
        <location filename="../qml/components/DetailsPanel.qml" line="213"/>
        <source>Show Content</source>
        <translation>Inhalt anzeigen</translation>
    </message>
</context>
<context>
    <name>FileDetail</name>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="13"/>
        <source>Summary</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="14"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="15"/>
        <source>Caption</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="16"/>
        <source>Collapsed</source>
        <translation>eingeklappt</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="17"/>
        <source>DB Size</source>
        <translation>DB-Größe</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="18"/>
        <source>DB Mod. Time</source>
        <translation>DB Änderungsdatum</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="19"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="20"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="21"/>
        <source>File mtime</source>
        <translation>Datei mtime</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="22"/>
        <source>DB path</source>
        <translation>DB-Pfad</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="23"/>
        <source>Mod. Time</source>
        <translation>Änderungszeit</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="24"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="25"/>
        <source>Charset</source>
        <translation>Zeichensatz</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="26"/>
        <source>PC Size</source>
        <translation>PC Größe</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="27"/>
        <source>DB udi</source>
        <translation>DB: udi</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="28"/>
        <source>DB aptg</source>
        <translation>DB: aptg</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="29"/>
        <source>Recipient</source>
        <translation>Empfänger</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="30"/>
        <source>Relevancy</source>
        <translation>Relevanz</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="31"/>
        <source>Sig</source>
        <translation>Sig</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="32"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../qml/components/FileDetail.qml" line="33"/>
        <source>Location</source>
        <translation>Speicherort</translation>
    </message>
</context>
<context>
    <name>LicenseListPart</name>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="119"/>
        <source>License text</source>
        <translation>Lizenztext</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Page Header</source>
        <translation type="vanished">Seitenkopf</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="28"/>
        <source>Refresh</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="31"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="42"/>
        <source>Advanced Search</source>
        <translation>Erweiterte Suche</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="49"/>
        <source>Hide search</source>
        <translation>Suche verbergen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="49"/>
        <source>Quick Search</source>
        <translation>Flotte Suche</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>ReJoll</source>
        <translation>ReJoll</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>%Ln hit(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation type="unfinished">
            <numerusform>%Ln Treffer</numerusform>
            <numerusform>%Ln Treffer</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="63"/>
        <source>something to search for</source>
        <translation>ein Suchbegriff</translation>
    </message>
</context>
<context>
    <name>Opal.About</name>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="55"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="84"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <source>Version %1 (%2)</source>
        <translation type="vanished">Version %1 (%2)</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="100"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="22"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="31"/>
        <source>Development</source>
        <translation>Entwicklung</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="103"/>
        <source>show contributors</source>
        <translation>Mitwirkende zeigen</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="105"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="51"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="57"/>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="47"/>
        <source>Homepage</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="108"/>
        <location filename="../qml/modules/Opal/About/private/ChangelogPage.qml" line="16"/>
        <source>Changelog</source>
        <translation>Änderungsverlauf</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="123"/>
        <source>Donations</source>
        <translation>Spenden</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="129"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="134"/>
        <source>show license(s)</source>
        <translation>
            <numerusform>Lizenz zeigen</numerusform>
            <numerusform>Lizenzen zeigen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="111"/>
        <source>Translations</source>
        <translation>Übersetzungen</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/AboutPageBase.qml" line="114"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="50"/>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="58"/>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="50"/>
        <source>Source Code</source>
        <translation>Quellcode</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="21"/>
        <source>Contributors</source>
        <translation>Mitwirkende</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="37"/>
        <source>Acknowledgements</source>
        <translation>Danksagungen</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="44"/>
        <source>Thank you!</source>
        <translation>Vielen Dank!</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ContributorsPage.qml" line="48"/>
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="31"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/InfoSection.qml" line="15"/>
        <source>show details</source>
        <translation>Details zeigen</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="25"/>
        <source>Download license texts</source>
        <translation>Lizenztexte herunterladen</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="31"/>
        <source>License(s)</source>
        <translation>
            <numerusform>Lizenz</numerusform>
            <numerusform>Lizenzen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicensePage.qml" line="41"/>
        <source>Note: please check the source code for most accurate information.</source>
        <translation>Hinweis: Bitte prüfen Sie den Quellcode für alle Einzelheiten.</translation>
    </message>
    <message>
        <source>Please refer to &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation type="vanished">Bitte beachten Sie &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="15"/>
        <source>External Link</source>
        <translation>Externer Link</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="32"/>
        <source>Open in browser</source>
        <translation>Im Browser öffnen</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="36"/>
        <source>Copied to clipboard: %1</source>
        <translation>In die Zwischenablage kopiert: %1</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/ExternalUrlPage.qml" line="41"/>
        <source>Copy to clipboard</source>
        <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/LicenseListPart.qml" line="120"/>
        <source>Please refer to &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>Bitte beachten Sie &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/ChangelogNews.qml" line="60"/>
        <source>News</source>
        <translation>Neuigkeiten</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/ChangelogNews.qml" line="61"/>
        <source>Changes since version %1</source>
        <translation>Änderungen seit Version %1</translation>
    </message>
</context>
<context>
    <name>Opal.About.Common</name>
    <message>
        <source>Author</source>
        <translation type="vanished">Autor</translation>
    </message>
    <message numerus="yes">
        <source>Author(s)</source>
        <translation type="vanished">
            <numerusform>Autor·in</numerusform>
            <numerusform>Autor·innen</numerusform>
        </translation>
    </message>
    <message>
        <source>Maintainer</source>
        <translation type="vanished">Maintainer·in</translation>
    </message>
    <message numerus="yes">
        <source>Maintainer(s)</source>
        <translation type="vanished">
            <numerusform>Maintainer·in</numerusform>
            <numerusform>Maintainer·innen</numerusform>
        </translation>
    </message>
    <message>
        <source>Programming</source>
        <translation type="vanished">Programmierung</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation type="vanished">Übersetzungen</translation>
    </message>
    <message>
        <source>Icon Design</source>
        <translation type="vanished">Symbol-Design</translation>
    </message>
    <message>
        <source>Third-party libraries</source>
        <translation type="vanished">Externe Bibliotheken</translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="vanished">Daten</translation>
    </message>
    <message>
        <source>Data License</source>
        <translation type="vanished">Daten-Lizenz</translation>
    </message>
    <message numerus="yes">
        <source>Data License(s)</source>
        <translation type="vanished">
            <numerusform>Daten-Lizenz</numerusform>
            <numerusform>Daten-Lizenzen</numerusform>
        </translation>
    </message>
    <message>
        <source>Terms of Use</source>
        <translation type="vanished">Nutzungsbedingungen</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="vanished">Schwedisch</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="vanished">Polnisch</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="vanished">Deutsch</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="vanished">Französisch</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="vanished">Chinesisch</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">Englisch</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="vanished">Italienisch</translation>
    </message>
    <message>
        <source>Finnish</source>
        <translation type="vanished">Finnisch</translation>
    </message>
    <message>
        <source>Norwegian</source>
        <translation type="vanished">Norwegisch</translation>
    </message>
    <message>
        <source>Latvian</source>
        <translation type="vanished">Lettisch</translation>
    </message>
    <message>
        <source>Estonian</source>
        <translation type="vanished">Estnisch</translation>
    </message>
    <message>
        <source>Czech</source>
        <translation type="vanished">Tschechisch</translation>
    </message>
    <message>
        <source>Greek</source>
        <translation type="vanished">Griechisch</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="vanished">Spanisch</translation>
    </message>
    <message>
        <source>Hungarian</source>
        <translation type="vanished">Ungarisch</translation>
    </message>
    <message>
        <source>Indonesian</source>
        <translation type="vanished">Indonesisch</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="vanished">Russisch</translation>
    </message>
    <message>
        <source>Dutch</source>
        <translation type="vanished">Niederländisch</translation>
    </message>
    <message>
        <source>Slovak</source>
        <translation type="vanished">Slowakisch</translation>
    </message>
    <message>
        <source>Turkish</source>
        <translation type="vanished">Türkisch</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/DonationsGroup.qml" line="7"/>
        <source>If you want to support my work, you can buy me a cup of coffee.</source>
        <translation>Sie können mir gerne einen Kaffee spendieren, wenn Sie meine Arbeit unterstützen möchten.</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/DonationsGroup.qml" line="8"/>
        <source>You can support this project by contributing, or by donating using any of these services.</source>
        <translation>Sie können dieses Projekt durch Ihre Mitarbeit oder durch eine Spende über einen dieser Dienste unterstützen.</translation>
    </message>
    <message>
        <location filename="../qml/modules/Opal/About/private/DonationsGroup.qml" line="9"/>
        <source>Your contributions to translations or code would be most welcome.</source>
        <translation>Ihre Mitarbeit bei Übersetzungen oder der Programmierung wäre eine große Hilfe.</translation>
    </message>
</context>
<context>
    <name>ResultItem</name>
    <message>
        <location filename="../qml/components/ResultItem.qml" line="49"/>
        <source>-- no title --</source>
        <comment>replacement for an empty field</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="92"/>
        <location filename="../qml/pages/SettingsPage.qml" line="93"/>
        <source>Enable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="102"/>
        <location filename="../qml/pages/SettingsPage.qml" line="103"/>
        <source>Disable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="111"/>
        <source>Start %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="119"/>
        <source>Stop %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="127"/>
        <source>Restart %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="134"/>
        <source>About</source>
        <translation type="unfinished">Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="145"/>
        <source>Settings</source>
        <comment>page title</comment>
        <translation type="unfinished">Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="149"/>
        <source>Previews</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="156"/>
        <source>Extract file contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="157"/>
        <source>If enabled, the app will try to generate text previews when fetching results. This may slow down result retrieval performance if %1 is large.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="157"/>
        <location filename="../qml/pages/SettingsPage.qml" line="170"/>
        <source>Batch Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="163"/>
        <source>Queries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="181"/>
        <source>Database Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="186"/>
        <source>Scheduler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="187"/>
        <source>%1, (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="188"/>
        <source>enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="188"/>
        <source>disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="192"/>
        <source>indexing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="193"/>
        <source>waiting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="194"/>
        <source>stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="195"/>
        <source>If enabled, the database will be updated twice a day, and 1 hour after a reboot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="207"/>
        <source>Indexer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="212"/>
        <source>Run now</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewFile</name>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="41"/>
        <source>Share %1…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="41"/>
        <source>File</source>
        <comment>argument for &apos;Share&apos;</comment>
        <translation type="unfinished">Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="53"/>
        <source>Share as Text…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="65"/>
        <source>Open…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="72"/>
        <location filename="../qml/pages/ViewFile.qml" line="73"/>
        <source>Copy %1 to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="72"/>
        <source>file path</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="unfinished">Dateipfad</translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="72"/>
        <location filename="../qml/pages/ViewFile.qml" line="73"/>
        <source>%1 copied to clipboard</source>
        <translation type="unfinished">%1 in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <location filename="../qml/pages/ViewFile.qml" line="73"/>
        <source>file contents</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
