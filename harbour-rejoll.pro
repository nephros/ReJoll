
TARGET = harbour-rejoll
CONFIG += sailfishapp sailfishapp_i18n
INCLUDEPATH += .

lupdate_only {
SOURCES += \
    qml/$${TARGET}.qml \
    qml/pages/*.qml \
    qml/cover/*.qml \
    qml/components/*.qml

}

# if we have a binary:
#SOURCES += src/main.cpp

TRANSLATIONS += translations/$${TARGET}-en.ts \
                translations/$${TARGET}-de.ts \
                translations/$${TARGET}-sv.ts \

desktop.files = $${TARGET}.desktop
desktop.path = $$PREFIX/share/applications
INSTALLS += desktop

qml.files = qml
qml.path = $$PREFIX/share/$${TARGET}

INSTALLS += qml

py.files = py/recolliface.py
py.path = $$PREFIX/share/$${TARGET}/py

INSTALLS += py

OTHER_FILES += $$files(rpm/*)

include(translations/translations.pri)
include(sailjail/sailjail.pri)
# must be last
include(icons/icons.pri)
