# -*- mode: sh -*-

# Firejail profile for /usr/bin/recoll

# x-sailjail-translation-catalog = rejoll
# x-sailjail-translation-key-description = permission-la-data
# x-sailjail-description = Recoll data storage
# x-sailjail-translation-key-long-description = permission-la-data_description
# x-sailjail-long-description = Store configuration and messages

### PERMISSIONS
# x-sailjail-permission = UserDirs

whitelist /usr/share/recoll
read-only /usr/share/recoll

noblacklist ${HOME}/.recoll
whitelist ${HOME}/.recoll
read-only ${HOME}/.recoll

noblacklist ${HOME}/.cache/recoll
whitelist ${HOME}/.cache/recoll
read-only ${HOME}/.cache/recoll

## D-Bus
dbus-user filter
dbus-user.talk org.freedesktop.DBus
dbus-user.call org.freedesktop.DBus=org.freedesktop.DBus@/*
dbus-user.broadcast org.freedesktop.DBus=org.freedesktop.DBus@/*

# BEG systemd manager and related
dbus-user.talk org.freedesktop.systemd1
dbus-user.call org.freedesktop.systemd1=org.freedesktop.systemd1@/*
#dbus-user.talk *=org.freedesktop.systemd1
dbus-user.call *=org.freedesktop.systemd1.Manager@/*
dbus-user.call *=org.freedesktop.systemd1.Service@/*
dbus-user.call *=org.freedesktop.systemd1.Unit@/*
dbus-user.call *=org.freedesktop.systemd1.Timer@/*
# END systemd manager and related

