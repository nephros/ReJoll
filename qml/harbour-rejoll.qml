// Copyright (c) 2023 Peter G. (nephros)
//
// SPDX-License-Identifier: Apache-2.0


import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import io.thp.pyotherside 1.5
import "pages"
import "cover"

ApplicationWindow {
    id: app

    allowedOrientations: Orientation.All

    /* detect closing of app*/
    signal willQuit()
    Connections { target: __quickWindow; onClosing: willQuit() }

    /*
    // Application goes into background or returns to active focus again
    onApplicationActiveChanged: {
        if (applicationActive) {
        } else {
        }
    }
    */

    Component.onCompleted: {
        // for sailjail
        Qt.application.domain  = "sailfish.nephros.org";
        Qt.application.version = "unreleased";
        console.info("Intialized", Qt.application.name, "version", Qt.application.version, "by", Qt.application.organization );
        console.debug("Parameters: " + Qt.application.arguments.join(" "))
        /*
         * this stopped working at some point as the key does not exist any more
         *
        // correct landscape for Gemini, set once on start
        allowedOrientations = (devicemodel === 'planetgemini')
            ? Orientation.LandscapeInverted
            : defaultAllowedOrientations
            */
    }

    // application settings:
    property alias getPreviews: config.getPreviews
    property alias maxResults:  config.maxResults
    ConfigurationGroup  {
        id: settings
        path: "/org/nephros/" + Qt.application.name
    }
    ConfigurationGroup  {
        id: config
        scope: settings
        path:  "app"
        property bool getPreviews: false
        property int  maxResults: 200
    }

    ListModel { id: results }

    function search(term )   { py.search(term, app.maxResults, app.getPreviews); }
    function loadMore(term, num) { py.search(term, num + results.count ); }

    Python { id: py
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../py'));
            importModule("recolliface", [ ], function(){} )
        }
        function search(term, max, preview) {
            call("recolliface.setupSearch", [ max, preview ], function(){
                call("recolliface.search", [ term ], function(res){
                    results.clear();
                    for (var i=0; i<res.length; i++) {
                        //console.debug("n: %1, t: %2, ip: %3, id: %4".arg(res[i].filename).arg(res[i].mtype).arg(res[i].ipath).arg(res[i].rcludi))
                        results.append(res[i]);
                    }
                    console.debug( "filled model:", results.count );
                })
            })
        }
    }
    initialPage: Component { MainPage{} }
    cover: CoverPage{}

    //PageBusyIndicator { running: app.status === Component.Loading }

}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
