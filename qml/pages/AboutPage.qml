// Copyright (c) 2023 Peter G. (nephros)
//
// SPDX-License-Identifier: Apache-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0 as S
import "../modules/Opal/About" as O

O.AboutPageBase { id: about

  allowedOrientations: S.Orientation.All

    appName: Qt.application.name
    appVersion:  Qt.application.version
    appIcon:  "image://theme/harbour-rejoll"
    description: "%1 is a Recoll frontend. Recoll finds documents based on their contents as well as their file names.".arg(appName)
    authors: "Peter G. (nephros)"
    sourcesUrl: "https://codeberg.org/nephros/harbour-rejoll"
    licenses: O.License { spdxId: "Apache-2.0" }

    //attributions: O.Attribution {}
    attributions: [
        O.OpalAboutAttribution {}
        //Attribution {
        //    name: "The Library"
        //    entries: ["1201 The Old Librarians", "2014 The Librarians"]
        //    licenses: License { spdxId: "CC0-1.0" }
        //}
    ]
    contributionSections: [
        O.ContributionSection {
            title:  qsTr("Translations")
            groups: [
                O.ContributionGroup {
                    title: qsTr("%1:",  "%1 is the native language name").arg(Qt.locale("de").nativeLanguageName)
                    entries: [ "nephros" ]
                },
                O.ContributionGroup {
                    title: qsTr("%1:",  "%1 is the native language name").arg(Qt.locale("sv").nativeLanguageName)
                    entries:  [ "eson" ]
                }
            ]
        }
    ]
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
