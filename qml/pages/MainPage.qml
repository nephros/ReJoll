// Copyright 2023 Peter G. (nephros) <sailfish@nephros.org>
//
// SPDX-License-Identifier: Apache-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page { id: page

    allowedOrientations: Orientation.All

    property bool unclutter: false

    property int progress: 0
    property string searchTerms

    DetailsPanel { id: detailInfo
        dock: Dock.Bottom
        modal: true
        animationDuration : 250
        //height: content.height
        height:  page.height * 2/3
        width: parent.width
    }
    PullDownMenu { id: pdp
        flickable: flick
         MenuItem { text: qsTr("About"); onClicked: { pageStack.push(Qt.resolvedUrl("AboutPage.qml")) } }
         MenuItem { text: qsTr("Settings"); onClicked: { pageStack.push(Qt.resolvedUrl("SettingsPage.qml")) } }
         /*
         MenuItem { text: showLocal ? qsTr("Hide %1", "show/hide local menu option").arg(qsTr("Local Installs", "menu option parameter")) : qsTr("Show %1", "show/hide local menu option").arg(qsTr("Local Installs", "menu option parameter")) ;
             // refresh on delayedclick otherwise the bounce animation freezes while we reload
             onClicked:      { showLocal = !showLocal }
             onDelayedClick: { updateHistory(page.historyfile,showLocal);  }
         }
         MenuItem { text: page.unclutter ? qsTr("Verbose Display", "menu option") : qsTr("Reduced Display", "menu option") ;
             onClicked: page.unclutter = !page.unclutter
         }
         */
         MenuItem { text: qsTr("Advanced Search") ;
             enabled: false
             //onClicked: {
             //    dateSearch.active = !dateSearch.active
             //    quickSearch.active = false
             //}
         }
         MenuItem { text: qsTr("Refresh");
             onDelayedClick: { app.search(searchTerms)  }
             visible: quickSearch.active
             enabled: searchTerms.length > 0
         }
         MenuItem { text: quickSearch.active ? qsTr("Hide search") : qsTr("Quick Search") ;
             visible: !quickSearch.active
             onDelayedClick: quickSearch.active = !quickSearch.active
         }
    }

    SilicaFlickable { id: flick
        anchors.fill: parent
        PageHeader { id: header ; title: qsTr("ReJoll") ; description: (results.count > 0) ? qsTr("%Ln hit(s)", "very, very unlikely to have only one, still, plurals please!",  results.count) : "…" }
        Column {
            id: searchBar
            anchors.top: header.bottom
            width: parent.width
            SearchField { id: quickSearch
                width: parent.width - Theme.horizontalPageMargin

                canHide: true
                onVisibleChanged: { focus = visible }
                placeholderText: qsTr("something to search for")
                inputMethodHints: Qt.ImhNoAutoUppercase

                EnterKey.enabled: text.length > 2
                EnterKey.iconSource: "image://theme/icon-m-search"
                EnterKey.onClicked: { page.searchTerms = text; app.search(text) }

                Separator { anchors.verticalCenter: parent.bottom; width: parent.width; color: Theme.primaryColor;}
            }
        }

        SilicaListView { id: view
            anchors.top: searchBar.bottom
            height: parent.height - (header.height + searchBar.height)
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            cacheBuffer: page.height * 2
            populate: Transition { NumberAnimation { properties: "y"; from: 100;  duration: 600 } }
            clip: true
            spacing: Theme.paddingMedium
            model: results
            highlight: highlightBar
            currentIndex: -1
            delegate: ResultItem{
                onClicked: {
                    detailInfo.details = results.get(index);
                    detailInfo.show();
                }
            }
            VerticalScrollDecorator {}
        }
    }

    Component { id: highlightBar
        Rectangle {
            color: Theme.rgba(Theme.highlightBackgroundColor, Theme.opacityFaint);
            border.color: Theme.highlightBackgroundColor
            radius: Theme.paddingSmall
        }
    }

}
// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
