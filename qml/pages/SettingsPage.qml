// Copyright (c) 2023 Peter G. (nephros)
//
// SPDX-License-Identifier: Apache-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.DBus 2.0
import Nemo.FileManager 1.0
import "../components"

Page { id: settingsPage

    allowedOrientations: Orientation.All

    //onStatusChanged: (status === PageStatus.Deactivating) ? app.refresh() : 0

    DiskUsage { id: usage }
    DBusInterface { // DBus systemd Timer
        id: tmrUnit
        bus: DBus.SessionBus
        property string unitName: "recollindex.timer" // would be lovely to use in path, but start/stop methods want it unescaped, and path wants it escaped...
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1/unit/recollindex_2etimer"
        iface: "org.freedesktop.systemd1.Unit"

        propertiesEnabled: true
        signalsEnabled: true

        property string unitFileState
        property string activeState
        property string subState

        function start() { dbus.enable(unitName); startstoptimer.start() }
        //function stop()  { dbus.disable(unitName) } // timer must be stopeed for disable call to work...
        function stop()  { stopOnly(); disabletimer.start() }
        function startOnly() { call("Start", "replace", undefined, undefined) }
        function stopOnly()  { call("Stop", "replace", undefined, undefined ) }
    }
    Timer { id: disabletimer
        running: false
        interval: 500
        repeat: false
        onRunningChanged: console.debug("timer:", running)
        onTriggered: {
            console.debug("triggered")
             if ( tmrUnit.unitFileState === "enabled" && tmrUnit.activeState === "inactive" ){  dbus.disable(tmrUnit.unitName); return }
        }
    }
    Timer { id: startstoptimer
        running: false
        interval: 500
        repeat: false
        onRunningChanged: console.debug("timer:", running)
        onTriggered: {
             console.debug("triggered")
             if ( tmrUnit.unitFileState === "enabled" && tmrUnit.activeState === "inactive" ){ tmrUnit.startOnly(); return }
             if ( tmrUnit.unitFileState === "disabled" && tmrUnit.activeState === "active" ) { tmrUnit.stopOnly(); return }
        }
    }
    DBusInterface { // DBus systemd Service
        id: svcUnit
        bus: DBus.SessionBus
        property string unitName: "recollindex.service" // would be lovely to use in path, but start/stop methods want it unescaped, and path wants it escaped...
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1/unit/recollindex_2eservice"
        iface: "org.freedesktop.systemd1.Unit"

        signalsEnabled: true
        propertiesEnabled: true

        property string unitFileState
        property string activeState
        property string subState

        function toggleRunning() {
            ((activeState === "active") && (subState === "running")) ? dbus.stop(unitName) : dbus.start(unitName)
            pageStack.pop()
        }
        Component.onCompleted: call('Subscribe', undefined); // get signals
    }

    DBusInterface { // DBus systemd Manager
        id: dbus
        bus: DBus.SessionBus
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1"
        iface: "org.freedesktop.systemd1.Manager"

        function enable(u) {
            //console.debug("dbus enable unit", u );
            typedCall('EnableUnitFiles', [
                { "type": "as", "value": [u] },
                { "type": "b", "value": false },
                { "type": "b", "value": true },
            ],
                function(result) { console.debug(qsTr("Enable %1").arg(u), JSON.stringify(result)); },
                function(result) { console.warn(qsTr("Enable %1").arg(u), result) }
            );
        }
        function disable(u) {
            //console.debug("dbus disable unit", u );
            typedCall('DisableUnitFiles', [
                { "type": "as", "value": [u] },
                { "type": "b", "value": false },
            ],
                function(result) { console.debug(qsTr("Disable %1").arg(u), JSON.stringify(result)); },
                function(result) { console.warn(qsTr("Disable %1").arg(u),  result) }
            );
        }
        function start(u) {
            //console.debug("dbus start unit", u );
            call('StartUnit',
                [u, "replace",],
                function(result) { console.debug("Job:", JSON.stringify(result)); },
                function(result) { console.warn(qsTr("Start %1").arg(u), result) }
            );
        }
        function stop(u) {
            console.debug("dbus stop unit", u );
            call('StopUnit',
                [u, "replace",],
                function(result) { console.debug("Job:", JSON.stringify(result)); },
                function(result) { console.warn(qsTr("Stop %1").arg(u), result) }
            );
        }
        function restart(u) {
            console.debug("dbus restart unit", u );
            call('RestartUnit',
                [u, "replace",],
                function(result) { console.debug("Job:", JSON.stringify(result)); },
                function(result) { console.warn(qsTr("Restart %1").arg(u), result) }
            );
        }
    }

    /*
    PullDownMenu {
        flickable: flick
         MenuItem { text: qsTr("About"); onClicked: { pageStack.push(Qt.resolvedUrl("AboutPage.qml")) } }
    }
    */

    SilicaFlickable { id: flick
        anchors.fill: parent
        contentHeight: col.height
        Column {
            id: col
            spacing: Theme.paddingSmall
            bottomPadding: Theme.itemSizeLarge
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            PageHeader{ title: qsTr("Settings", "page title")}
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Previews")
            }
            TextSwitch{ id: getPreviewSwitch
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.getPreviews
                automaticCheck: true
                text: qsTr("Extract file contents")
                description: qsTr("If enabled, the app will try to generate text previews when fetching results. This may slow down result retrieval performance if %1 is large.").arg(qsTr("Batch Size"))
                onClicked: app.getPreviews = checked
            }
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Queries")
            }
            Slider {
                id: maxResSlider
                handleVisible: enabled
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                label: qsTr("Batch Size");
                minimumValue: 5
                maximumValue: 500
                stepSize: 5
                valueText: sliderValue
                value: app.maxResults
                onReleased: app.maxResults = sliderValue
            }
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Database Index")
            }
            TextSwitch{ id: tmrsw
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Scheduler") + ": " + qsTr("%1").arg( (tmrUnit.unitFileState === "enabled") ? qsTr("enabled") : qsTr("disabled"))
                description: qsTr("If enabled, the database will be updated twice a day, and 1 hour after a reboot")
                checked: ((tmrUnit.unitFileState === "enabled") && (tmrUnit.activeState === "active"))
                busy: (svcUnit.subState === "running")
                automaticCheck: false
                onClicked: checked ? tmrUnit.stop() : tmrUnit.start()
            }

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                DetailItem {
                    anchors.verticalCenter: parent.verticalCenter
                    width: parent.width/2
                    label: qsTr("Indexer")
                    value: qsTr("%1", "index service state").arg( (svcUnit.subState === "running") ? qsTr("indexing") : qsTr("waiting"))
                }
                SecondaryButton {
                    anchors.verticalCenter: parent.verticalCenter
                    text:      qsTr("Run now")
                    enabled: (svcUnit.subState !==  "running")
                    onClicked: svcUnit.toggleRunning()
                }
            }
            CollapsingHeader {
                target: confcol
                text: qsTr("Recoll Configuration")
                fontSize: Theme.fontSizeMedium
                BackgroundItem { anchors.fill: parent;
                    onClicked: { parent.target.visible = ! parent.target.visible; }
                }
            }
            Column { id: confcol
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                visible: false
                DetailItem { id: confloc
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    alignment: Qt.AlignLeft
                    label: qsTr("Configuration Location")
                    Component.onCompleted: {
                        py.call("recolliface.getConfDir", [], function(res) { value = res } )
                    }
                }
                ButtonLayout {
                    Button {
                        text: qsTr("Open Directory")
                        onClicked: Qt.openUrlExternally(Qt.resolvedUrl(confloc.value))
                    }
                    Button {
                        text: qsTr("Open Config")
                        onClicked: Qt.openUrlExternally(Qt.resolvedUrl(confloc.value + "/recoll.conf"))
                    }
                }
                /*
                DetailItem { id: cacheloc
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    alignment: Qt.AlignLeft
                    label: qsTr("Cache Location")
                    value: path + " (" + Format.formatFileSize(size) + ")"
                    property string path
                    property int size
                    onPathChanged: usage.calculate([ path ],
                        function(s) { for ( var k in s) { cacheloc.size = s[k] }
                        })
                    Component.onCompleted: {
                        py.call("recolliface.getConfParam", ["cachedir"], function(res) { path = res } )
                    }
                }
            DetailItem {
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                label: qsTr("Data Location")
                alignment: Qt.AlignLeft
                Component.onCompleted: {
                   py.call("recolliface.getDataDir", [], function(res) { value = res } )
                }
            }
            */
                DetailItem { id: dbloc
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    alignment: Qt.AlignLeft
                    label: qsTr("Database")
                    value: path + "/" + name + " (" + Format.formatFileSize(size) + ")"
                    property string name
                    property string path
                    property int size
                    //onNameChanged: usage.calculate([ path + "/" + name], function(s) {size = s; console.debug(JSON.stringify(s))})
                    onPathChanged: usage.calculate([ path + "/" + name],
                        function(s) { for ( var k in s) { dbloc.size = s[k] } })
                    Component.onCompleted: {
                        py.call("recolliface.getConfParam", ["cachedir"], function(res) { path = res } )
                        py.call("recolliface.getConfParam", ["dbdir"], function(res) { name = res } )
                    }
                }
                DetailItem {
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    alignment: Qt.AlignLeft
                    label: qsTr("Topdirs")
                    Component.onCompleted: {
                        py.call("recolliface.getConfParam", ["topdirs"], function(res) { value = res.split(" ").join(", ") } )
                    }
                }
                DetailItem {
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    alignment: Qt.AlignLeft
                    label: qsTr("Skipped Names")
                    Component.onCompleted: {
                        var names = ""
                        py.call("recolliface.getConfParam", ["skippedNames"], function(res) { names = res.split(" ").join(", ") } )
                        py.call("recolliface.getConfParam", ["skippedNames+"], function(res) { value = names + ", " + res.split(" ").join(", ") } )
                        value = names
                    }
                }
                DetailItem {
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    alignment: Qt.AlignLeft
                    label: qsTr("Skipped Paths")
                    Component.onCompleted: {
                        py.call("recolliface.getConfParam", ["skippedPaths"], function(res) { value = res.split(" ").join(", ") } )
                    }
                }
            }

         }

        VerticalScrollDecorator {}
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
