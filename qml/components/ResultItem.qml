// Copyright (c) 2023 Peter G. (nephros)
//
// SPDX-License-Identifier: Apache-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.FileManager 1.0

ListItem { id: root
    contentHeight: resultCol.height

    FileInfo { id: file }
    Component.onCompleted: file.url = url

    menu: ContextMenu {
        MenuItem { text: qsTr("Open"); onClicked: openFile() }
        MenuItem { text: qsTr("Filemanager"); onClicked: openDir(); }
    }
    function openFile() {
        console.info("Opening", url)
        Qt.openUrlExternally(url)
    }
    function openDir() {
        console.info("Opening", file.directoryPath)
        Qt.openUrlExternally(Qt.resolvedUrl(file.directoryPath))
    }

    Column { id: icon
        anchors.top: parent.top
        anchors.left: parent.left
        width: Theme.itemSizeLarge
        spacing: Theme.paddingSmall
        rightPadding: Theme.paddingMedium
        Image {
            source: Theme.iconForMimeType(mtype)
            height: Theme.iconSizeMedium
            width:  Theme.iconSizeMedium
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Label{
            text: Format.formatFileSize(fbytes)
            color: Theme.secondaryColor
            font.pixelSize: Theme.fontSizeTiny
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Label{
            text: mtype
            color: Theme.secondaryColor
            font.pixelSize: Theme.fontSizeTiny;
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            elide: Text.ElideLeft
        }
    }
    Column { id: resultCol
        //anchors.verticalCenter: icon.verticalCenter
        anchors.left: icon.right
        width: parent.width - icon.width
        spacing: Theme.paddingSmall
        // we always(?) have a file name:
        Label{ color: Theme.highlightColor; width: parent.width; font.pixelSize: Theme.fontSizeSmall;
            text: filename
        }
        // ... but only sometimes a title
        Label{ width: parent.width;
            text: (title.length > 0) ? title : qsTr('-- no title --', 'replacement for an empty field')
            color: (title.length > 0) ? Theme.primaryColor : Theme.secondaryColor
        }
        Label{ color: Theme.secondaryColor; width: parent.width; font.pixelSize: Theme.fontSizeTiny;
            text: url.substring(0,url.lastIndexOf('/')).replace('file://', '').replace(/^\/home\/[^/]+/, '~')
            elide: Text.ElideLeft
            //truncationMode:TruncationMode.Fade
        }
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
