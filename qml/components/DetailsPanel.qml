// Copyright (c) 2023 Peter G. (nephros)
//
// SPDX-License-Identifier: Apache-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0

DockedPanel { id: panel
    z: 10 // this fixes transparency and focus problems

    property var details
    onDetailsChanged: {
        console.debug(JSON.stringify(details))
    }

    Rectangle {
        clip: true
        anchors.fill: parent
        anchors.centerIn: parent
        radius: Theme.paddingSmall
        property color backgroundColor: !app.isSessionBus ? Theme.rgba(Theme.highlightDimmerFromColor(Theme.errorColor, Theme.colorScheme), Theme.opacityOverlay) : Theme.secondaryHighlightColor
        gradient: Gradient {
            GradientStop { position: 0; color: Theme.rgba(Theme.highlightDimmerFromColor(backgroundColor, Theme.colorScheme), Theme.opacityOverlay) }
            GradientStop { position: 2; color: Theme.rgba(Theme.overlayBackgroundColor, Theme.opacityOverlay) }
        }

    }
    Separator {
        anchors {
            verticalCenter: parent.top
            horizontalCenter: parent.horizontalCenter
            bottomMargin: Theme.paddingMedium
        }
        width: parent.width ; height: Theme.paddingSmall
        color: Theme.lightPrimaryColor;
        horizontalAlignment: Qt.AlignHCenter
    }

    Loader { id: loader
        active: parent.visible //&& parent.details !== undefined
        width: parent.width
        anchors.fill: parent
        anchors.top: parent.top
        sourceComponent:  SilicaFlickable { id: pflick
            contentHeight: content.height
            Flow { id: content
                width: parent.width
                bottomPadding: Theme.paddingLarge
                Label { id: desc
                    width: parent.width
                    anchors.topMargin: Theme.paddingMedium
                    anchors.leftMargin: Theme.horizontalPageMargin
                    anchors.rightMargin: Theme.horizontalPageMargin
                    text: details["abstract"]
                    color: Theme.secondaryColor
                    //font.pixelSize: Theme.fontSizeLarge
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter
                }
                Label { id: prev
                    visible: (detailsText && (detailsText.length > 0))
                    width: parent.width
                    anchors.topMargin: Theme.paddingMedium
                    anchors.leftMargin: Theme.horizontalPageMargin
                    anchors.rightMargin: Theme.horizontalPageMargin
                    text: detailsText ? detailsText : ""
                    color: Theme.secondaryColor
                    //font.pixelSize: Theme.fontSizeLarge
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter
                }
                // Generic file information
                Column { id: filecol
                    width: isPortrait ? parent.width : parent.width/2
                    spacing: 0

                    SectionHeader { text: qsTr("File Properties") }
                    //FileDetail { label: qsTr("Other Names"); visible: value.length > 0;   value: (unit.names.length > 1) ? unit.names.splice(0,1) : "" }
                    Repeater { id: keyrep

                        width: parent.width
                        anchors.topMargin: Theme.paddingMedium
                        anchors.leftMargin: Theme.horizontalPageMargin
                        anchors.rightMargin: Theme.horizontalPageMargin
                        // FIXME: move these into a general place, not a delegate!!!
                        readonly property var fallbackKeys: knownKeys["text/plain"]
                        /*
                        readonly property var knownKeys: {
                            "application/epub+zip":      [ "abstract","caption","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url","keywords"],
                            "application/javascript":    [ "abstract","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url","dmtime" ],
                            "application/pdf":           [ "abstract","author","caption","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url","rclmbreaks",],
                            "application/x-rpm":         [ "abstract","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url" ],
                            "application/x-shellscript": [ "abstract","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url" ],
                            "audio/ogg":                 [ "abstract","caption","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url","author" ],
                            "image/jpeg":                [ "abstract","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url","author","caption","collapsecount","dmtime","recipient", ],
                            "image/png":                 [ "abstract","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url" ],
                            "image/svg+xml":             [ "abstract","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url" ],
                            "image/vnd.djvu":            [ "abstract","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url" ],
                            "image/x-xcf":               [ "abstract","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url" ],
                            "inode/directory":           [ "abstract","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url" ],
                            "message/rfc822":            [ "abstract","author","caption","dbytes","dmtime","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","recipient","relevancyrating","sig","title","url",],
                            "text/css":                  [ "abstract","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url" ],
                            "text/html":                 [ "abstract","caption","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url","rclaptg",],
                            "text/plain":                [ "abstract","dbytes","fbytes","filename","fmtime","ipath","mtime","mtype","origcharset","pcbytes","rcludi","relevancyrating","sig","title","url",],

                            //"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                            //"application/vnd.sun.xml.writer"
                        }
                        */

                        readonly property var knownKeys: {
                            "application/epub+zip":      [ "abstract","caption","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url","keywords"],
                            "application/javascript":    [ "abstract","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url","dmtime" ],
                            "application/pdf":           [ "abstract","author","caption","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url","rclmbreaks",],
                            "application/x-rpm":         [ "abstract","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url" ],
                            "application/x-shellscript": [ "abstract","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url" ],
                            "audio/ogg":                 [ "abstract","caption","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url","author" ],
                            "image/jpeg":                [ "abstract","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url","author","caption","collapsecount","recipient", ],
                            "image/png":                 [ "abstract","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url" ],
                            "image/svg+xml":             [ "abstract","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url" ],
                            "image/vnd.djvu":            [ "abstract","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url" ],
                            "image/x-xcf":               [ "abstract","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url" ],
                            "inode/directory":           [ "abstract","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url" ],
                            "message/rfc822":            [ "abstract","author","caption","fbytes","filename","mtime","mtype","origcharset","recipient","relevancyrating","title","url",],
                            "text/css":                  [ "abstract","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url" ],
                            "text/html":                 [ "abstract","caption","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url","rclaptg",],
                            "text/plain":                [ "abstract","fbytes","filename","mtime","mtype","origcharset","relevancyrating","title","url",],

                            //"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                            //"application/vnd.sun.xml.writer"
                        }

                        model: (knownKeys[details.mtype]) ? knownKeys[details.mtype] : fallbackKeys
                        delegate: FileDetail { rawlabel: modelData; rawvalue: details[modelData] }
                    }

                    Separator {color: Theme.secondaryHighlightColor; width: parent.width; horizontalAlignment: Qt.AlignRight }
                }
/*
                Loader { id: timerLoader
                    active: pflick.detailsType == "timer"
                    width: isPortrait ? parent.width : parent.width/2
                    sourceComponent: Column {
                        SectionHeader { text: qsTr("Timer Properties") }
                        FileDetail { label: "Persistent";                 value: tmr.persistent}
                        FileDetail { label: "Wake System";                value: tmr.wakeSystem}
                        FileDetail { label: "Remain After Elapse";        value: tmr.remainAfterElapse}
                        // this is always "OnCalendar" - "for now"
                        FileDetail { label: tmr.timersCalendar[0][0];
                            value: {
                                const s = [];
                                for (var i=0; i<tmr.timersCalendar.length;i++){
                                    s.push(tmr.timersCalendar[i][1])
                                }
                                return s.join('\n');
                            }
                        }
                        FileDetail { label: "Next Elapse";          value: (tmr.nextElapseUSecRealtime != 0) ? new Date(Math.floor(tmr.nextElapseUSecRealtime/1000)) : "never" }
                        FileDetail { label: "Last Trigger";         value: (tmr.lastTriggerUSec != 0) ? new Date(Math.floor(tmr.lastTriggerUSec/1000)) : "never" }
                        //FileDetail { label: "Accuracy";             value: (tmr.accuracyUSec/1000000).toFixed(2)}
                        //FileDetail { label: "Randomized Delay";     value: (tmr.randomizedDelayUSec/1000000).toFixed(2)}
                        FileDetail { label: "Accuracy";             value: Math.round(tmr.accuracyUSec/1000000)+"s"}
                        FileDetail { label: "Randomized Delay";     value: Math.round(tmr.randomizedDelayUSec/1000000)+"s"}
                        FileDetail { label: "Fixed Random Delay";   value: tmr.fixedRandomDelay}
                        FileDetail { label: "Result";               value: tmr.result.toUpperCase()}
                    }
                }
                Loader { id: socketLoader
                    active: pflick.detailsType == "socket"
                    width: isPortrait ? parent.width : parent.width/2
                    sourceComponent: Column {
                        SectionHeader { text: qsTr("Socket Properties") }
                        //FileDetail { label: "Type";             value: soc.type }
                        FileDetail { label: "IPv6 only";        value: soc.bindIPv6Only }
                        FileDetail { label: "Device";           value: soc.bindToDevice}
                        FileDetail { label: "Listen";           value: soc.listen.map(function(e) { return e[0] + " " + e[1] }).join("\n") }
                        FileDetail { label: "Service";          value: soc.service}
                        //FileDetail { label: "Accept";           value: soc.accept}
                        FileDetail { label: "Connections";      value: soc.nConnections}
                        FileDetail { label: "Accepted";         value: soc.nAcceped}
                        FileDetail { label: "Refused";          value: soc.nRefused}
                        FileDetail { label: "Result";           value: soc.result.toUpperCase()}
                    }
                }
                Loader { id: mountLoader
                    active: pflick.detailsType == "mount"
                    width: isPortrait ? parent.width : parent.width/2
                    sourceComponent: Column {
                        SectionHeader { text: qsTr("Mount Properties") }
                        FileDetail { label: "Type";           value: mnt.type }
                        FileDetail { label: "Where";          value: mnt.where }
                        FileDetail { label: "What";           value: mnt.what}
                        FileDetail { label: "Options";        value: mnt.options}
                        FileDetail { label: "Result";         value: mnt.result.toUpperCase()}
                    }
                }
                Loader { id: pathLoader
                    active: pflick.detailsType == "path"
                    width: isPortrait ? parent.width : parent.width/2
                    sourceComponent: Column {
                        SectionHeader { text: qsTr("Path Properties") }
                        //FileDetail { label: "Paths";          value: JSON.stringify(pth.paths) }
                        FileDetail { label: "Paths";
                            value: {
                                const s = [];
                                for (var i=0; i<pth.paths.length;i++){
                                    s.push(pth.paths[i][0] + ': ' + pth.paths[i][1])
                                }
                                return s.join('\n');
                            }
                        }
                        FileDetail { label: "Make Directory"; value: pth.makeDirectory}
                        //FileDetail { label: "Mode";           value: pth.directoryMode}
                        FileDetail { label: "Result";         value: pth.result.toUpperCase()}
                    }
                }
*/
                Column { id: buttcol // teehehehehee!
                    width: isPortrait ? parent.width : parent.width/2
                    spacing: 0
                    SectionHeader { text: qsTr("File") }
                    ButtonLayout { id: buttons
                        width: parent.width
                        property string filePath: details.url
                        SecondaryButton {
                            text: qsTr("Copy Path")
                            icon.source: "image://theme/icon-m-clipboard"
                            enabled: parent.filePath.length > 0
                            onClicked: {
                                Clipboard.text = parent.filePath;
                                app.popup(qsTr("%1 copied to clipboard").arg(qsTr("file path", "parameter for 'copied to clipboard' popup message")), parent.filePath);
                            }
                        }
                        SecondaryButton {
                            text: qsTr("Show Content")
                            icon.source: (Theme.colorScheme == Theme.LightOnDark) ? "image://theme/icon-m-file-document-light" : "image://theme/icon-m-file-document-dark"
                            enabled: parent.filePath.length > 0
                            onClicked: {
                                pageStack.push(Qt.resolvedUrl("../pages/ViewFile.qml"), { "fileName": details.filename , "fileType": details.mtype, "filePath":  parent.filePath } )
                            }
                        }
                    }
                }
            }
        }
    }

}
// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
