// Copyright (c) 2023 Peter G. (nephros)
// SPDX-License-Identifier: Apache-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0

DetailItem {
    alignment: Qt.AlignLeft
    readonly property string breaker: "NNNN-NN-NNTTT:TT:TT" // a date string
    forceValueBelow: value.length > breaker.length
    leftMargin: Theme.paddingLarge
    readonly property var l10nKeys: {
        "abstract":         qsTr("Summary"),
        "author":           qsTr("Author"),
        "caption":          qsTr("Caption"),
        "collapsecount":    qsTr("Collapsed"),
        "dbytes":           qsTr("DB Size"),
        "dmtime":           qsTr("DB Mod. Time"),
        "fbytes":           qsTr("Size"),
        "filename":         qsTr("Name"),
        "fmtime":           qsTr("File mtime"),
        "ipath":            qsTr("DB path"),
        "mtime":            qsTr("Mod. Time"),
        "mtype":            qsTr("Type"),
        "origcharset":      qsTr("Charset"),
        "pcbytes":          qsTr("PC Size"),
        "rcludi":           qsTr("DB udi"),
        "rclaptg":          qsTr("DB aptg"),
        "recipient":        qsTr("Recipient"),
        "relevancyrating":  qsTr("Relevancy"),
        "sig":              qsTr("Sig"),
        "title":            qsTr("Title"),
        "url":              qsTr("Location"),
    }
    property string rawvalue
    property string rawlabel
    onRawlabelChanged: {
        label = l10nKeys[rawlabel] ? l10nKeys[rawlabel] : rawlabel
    }
    onRawvalueChanged: {
        if (rawvalue === "")                { value = "-"
        } else if (/time$/.test(rawlabel))  { value = Format.formatDate(new Date(1000*rawvalue),Formatter.DateFull)
        } else if (/bytes$/.test(rawlabel)) { value =  Format.formatFileSize(rawvalue)
        } else if (rawlabel === "url")      { value = rawvalue.replace('file://','')
        } else { value = rawvalue }
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
