#!/usr/bin/python3

# Copyright (c) 2023 Peter G. (nephros)
# SPDX-License-Identifier: Apache-2.0

from recoll import recoll, rclextract, rclconfig
import pyotherside

_maxResults = 200
_getPreviews = False

def setupSearch(num, preview):
    global _maxResults
    _maxResults=num
    global _getPreviews
    _getPreviews=preview

def search(term):
    db = recoll.connect()
    query = db.query()
    nres = query.execute(term, fetchtext=_getPreviews)
    results = query.fetchmany(_maxResults)
    model = list()
    #print("%s", results[0].keys())
    for doc in results:
        entry = doc.items()
        entry["preview"] = ""
        if (_getPreviews and (len(doc.text) > 0)):
            entry["preview"] = doc.text
        model.append(entry)
    return model

def getConfDir():
    conf = rclconfig.RclConfig()
    return conf.getConfDir()

def getDataDir():
    conf = rclconfig.RclConfig()
    return conf.getDataDir()

def getConfParam(key):
    conf = rclconfig.RclConfig()
    return conf.getConfParam(key)

